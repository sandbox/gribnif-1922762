<?php
/**
 * @file form functions for the date_repeat_sequence module
 */
/**
 * Validates all date repeat fileds and creates values for use on submit
 */
function _date_repeat_sequence_validate($element, &$form_state) {
  $field_name = $element['#field_name'];
  $values = $form_state['values'][$field_name][$element['#language']];
  // Save only the first value in the date field. We will create all the other nodes on submit.
  $value[] = array_shift($values);

  $form_state['values'][$field_name][$element['#language']] = $value;

  // Submit buttons can add extraneous data to the array, so remove it.
  foreach (array_keys($values) as $key) {
    if (!is_numeric($key)) {
      unset($values[$key]);
    }
  }
  $sequence = array('#parents' => array('date_repeat_sequence', $field_name));
  form_set_value($sequence, $values, $form_state);
}

/**
 * Generate the form API edit options that are part of the fields on date repeat sequence nodes
 *
 * @return
 *   The form API array for the buttons
 */
function _date_repeat_sequence_edit_options($field) {
  $field_name = $field['field_name'];
  $options = array(
    'only' => t('Only this occurrence'),
    'future' => t('This and all future occurrences'),
    'all' => t('All occurrences'),
  );
  return array(
    '#weight' => 100,
    '#tree' => FALSE,
    "date_repeat_sequence_edit__$field_name" => array(
      '#type' => 'radios',
      '#title' => t('Sequence editing options'),
      '#options' => $options,
      '#default_value' => empty($field['settings']['sequence']['edit_default']) ? 'all' : $field['settings']['sequence']['edit_default'],
      '#required' => TRUE,
      '#attributes' => array('class' => array('sequence-save-options',  'seq-save-' . $field_name)),
    ),
  );
}
