<?php
/**
 * @file - Page callbacks
 */

/**
 * node/1234/repeats page callback
 *
 * @TODO Fix the remove from seequence portion
 */
function date_repeat_sequence_page($node) {
  $out = '';
  if (empty($node->date_repeat_sequences)) {
    return date_repeat_field_page('node', $node);
  }
  $lang = empty($node->language) ? LANGUAGE_NONE : $node->language;
  foreach ($node->date_repeat_sequences as $seq) {
    drupal_set_title(t('Date Repeats for !title', array('!title' => $node->title)));
    $title = NULL;
    $row = array();
    $rows = array();
    $headers = array(
      t('Date'),
      t('Edit !type', array('!type' => node_type_get_name($node))),
      //t('Remove From Sequence'),
      t('Delete'),
    );
    $instance = field_info_instance('node', $seq['field'], $node->type);
    $field = field_info_field($seq['field']);
    $format = date_format_type_format($instance['display']['default']['settings']['format_type']);
    $timezone = date_get_timezone($field['settings']['tz_handling']);
    $timezone_db = date_get_timezone_db($field['settings']['tz_handling']);
    $title = '<h4>' . t('Repeats for !field', array('!field' => $instance['label'])) . '</h4>';
    $title .= '<div>' . date_repeat_rrule_description($node->{$seq['field']}[$lang][0]['rrule']) . '</div>';
    $sql_info = $field['storage']['details']['sql'][FIELD_LOAD_REVISION];
    reset($sql_info);
    $table = key($sql_info);
    $column = array_shift($sql_info[$table]);
    $sql = 'SELECT n.nid, c.' . $column . ' date FROM {date_repeat_sequence_node} dn
      JOIN {node} n ON n.nid = dn.nid
      JOIN {' . $table . '} c ON c.revision_id = n.vid
      WHERE dn.sqid = :sqid
      ORDER BY c.' . $column . ' ASC';
    $results = db_query($sql, array(':sqid' => $seq['sqid']));

    foreach ($results as $result) {
      $row = array();

      $date = new DateObject($result->date, $timezone_db);
      $row[] = l($date->format($format), 'node/' . $result->nid);
      $row[] = l(t('Edit'), 'node/' . $result->nid . '/edit');
      //$row[] = l('Remove', 'node/' . $result->nid . '/seq_remove');
      $row[] = l(t('Delete'), 'node/' . $result->nid . '/delete');
      $rows[] = $row;
    }

    $out .= $title . theme('table', array('header' => $headers, 'rows' => $rows));
  }
  return $out;
}

/**
 * Menu callback -- ask for confirmation of node deletion
 */
function date_repeat_node_delete_confirm(&$form_state, $node) {
  $form['node'] = array(
    '#type' => 'value',
    '#value' => $node,
  );
  if (date_repeat_sequence_node($node)) {
    module_load_include('inc', 'date_repeat_sequence', 'date_repeat_sequence_form');
    foreach (element_children($node) as $field) {
      if (date_repeat_sequence_is_field($field, $node) && isset($node->date_repeat_sequences[$field])) {
        $field_settings = field_info_instance('node', $field, $node->type);
        if ($field_settings['settings']['sequence']['show_edit']) {
          $form['edit_options'][$field] = _date_repeat_sequence_edit_options($field_settings);
          $form['edit_options'][$field]['options']['#title'] = $field_settings['label'] . ' ' . $form['edit_options'][$field]['options']['#title'];
        }
        else {
          $form["date_repeat_sequence_edit__$field"] = array(
            '#type' => 'value',
            '#value' => $field_settings['settings']['sequence']['edit_default'],
          );
        }
      }
    }
  }
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $node->title)),
    isset($_GET['destination']) ? $_GET['destination'] : 'node/' . $node->nid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute node deletion
 */
function date_repeat_node_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $node = $form_state['values']['node'];
    foreach (element_children($node) as $field) {
      if (date_repeat_sequence_is_field($field, $node) && isset($node->date_repeat_sequences[$field])) {
        $edit_name = "date_repeat_sequence_edit__$field";
        if (empty($form_state['values'][$edit_name])) {
          node_delete($node->nid);
        }
        else {
          $node = $form_state['values']['node'];
          $node->$edit_name = $form_state['values'][$edit_name];
          module_load_include('inc', 'date_repeat_sequence', 'date_repeat_sequence.node');
          _drs_node_delete($node);
        }
      }
    }
  }

  $form_state['redirect'] = '<front>';
}
