Drupal 7 Notes:

  This is a port of the original Drupal 6 module by Amherst College. The only major difference is that now all of the field settings are per instance, rather than being (partially) set globally for all instances of a particular field. However, due to this change, migrating nodes from D6 to D7 will reset all of the Date Repeat Sequence settings to their default values.
  

Original README:

This modules development was done by Causecast <http://causecast.org> and
it's maintainer, an employee of Causecast, Adam Gregory (arcaneadam) <http://drupal.org/user/234551>

INSTALL

To install simply drag the module into your sites/all/modules folder and enable it.

USAGE

To use simply add a Date CCK field to any node and enable repeat options for that field.
On the field settings page there is a fieldset for Date Sequencing. Open that and enable the checkbox to use Date Repeat Sequencing
Once enabled all nodes that utilize a Repeat will now have child nodes created as part of a sequence.

To access the Repeat sequences for any particular node simple click the Repeat tab, just as you would with a regular Date Repeat.

You can use more then one date repeat field with or without sequencing enabled on any node so the module can handle the ability to be part of more then one sequence.

MODAL Confirmation

To use a Modal confirm message to verify how the user wants to change the sequence simply enable jquery UI and set the appropriate option in the field settings page.