<?php
/**
 * @file nodeapi functions for date_repeat_sequence
 */
/**
 * Updates a nodes date_repeats sequences
 */
function _date_repeat_sequence_node_update(&$node) {
  $sequences = $node->date_repeat_sequence;
  foreach ($sequences as $field => $values) {
    if (empty($node->date_repeat_sequences[$field])) {
      if (empty($values)) {
        //if there are no values then don't do anything
        break;
      }
      _date_repeat_sequence_node_insert($node, $field);
    }
    else {
      $field_settings = field_info_instance('node', $field, $node->type);
      $edit_name = "date_repeat_sequence_edit__$field";
      $update_op = empty($node->$edit_name) ?  $field_settings['settings']['sequence']['edit_default'] : $node->$edit_name;
      $function = "_date_repeat_sequence_node_update_" . $update_op;
      $args = array(
        $node,
        $field,
      );
      call_user_func_array($function, $args);
    }
  }
}

/**
 * Inserts a nodes date_repeats sequences
 *
 * @param $node
 *   The node object.
 *
 * @param $field_name
 *   The field name to insert a sequence for. If none is specified then all sequence fields will be inserted.
 */
function _date_repeat_sequence_node_insert(&$node, $field_name = NULL) {
  $sequences = $node->date_repeat_sequence;
  if (!empty($field_name) && isset($sequences[$field_name])) {
    $seq = array(
      'pnid' => $node->nid,
      'field' => $field_name,
    );
    drupal_write_record('date_repeat_sequence', $seq);
    $node->date_repeat_sequences[$seq['field']] = $seq;
    $record = array(
      'sqid' => $seq['sqid'],
      'nid' => $node->nid,
    );
    drupal_write_record('date_repeat_sequence_node', $record);
    _date_repeat_sequence_generate_nodes($seq, $node);
    return;
  }
  foreach ($sequences as $field => $values) {
    if (count($values) < 1) {
      continue;
    }
    $seq = array(
      'pnid' => $node->nid,
      'field' => $field,
    );
    drupal_write_record('date_repeat_sequence', $seq);
    $node->date_repeat_sequences[$seq['field']] = $seq;
    $record = array(
      'sqid' => $seq['sqid'],
      'nid' => $node->nid,
    );
    drupal_write_record('date_repeat_sequence_node', $record);
    _date_repeat_sequence_generate_nodes($seq, $node);
  }
}

/**
 * Generates nodes that are a part of a sequence.
 */
function _date_repeat_sequence_generate_nodes($seq, $node) {
  $sequences = $node->date_repeat_sequence;
  $field = $seq['field'];
  $lang = empty($node->language) ? LANGUAGE_NONE : $node->language;
  foreach ($sequences[$field] as $occurrence) {
    $new_node = clone $node;
    $new_node->$field = array($lang => array($occurrence));
    unset($new_node->date_repeat_sequence);
    $new_node->nid = 0;
    $save_node->is_new = TRUE;
    unset($new_node->vid);
    //If auto_nodetitle is used we unset the flag so the title gets regenerated
    unset($new_node->auto_nodetitle_applied);
    node_save($new_node);
    $record = array(
      'sqid' => $seq['sqid'],
      'nid' => $new_node->nid,
    );
    drupal_write_record('date_repeat_sequence_node', $record);
    unset($new_node);
  }
}

/**
 * Saves an existing repeat sequence for all nodes in the sequence
 *
 * @param $node
 *   The Node object
 *
 * @param $field
 *   The sequence Field name
 */
function _date_repeat_sequence_node_update_all($node, $field) {
  //Set some defaults
  $new_nodes = $nodes = $ready_nodes = $field_values = array();
  //Get some variables ready
  $sqid = $node->date_repeat_sequences[$field]['sqid'];
  $pnid = $node->date_repeat_sequences[$field]['pnid'];
  $nids = date_repeat_sequence_load_nids($sqid);
  $values = $node->date_repeat_sequence[$field];
  $rev = !empty($node->revision);
  $lang = empty($save_node->language) ? LANGUAGE_NONE : $save_node->language;
  if ($pnid != $node->nid) {
    //In the case where the user changed the time or end of an event in a non-parent node then we need to regenerate the
    //Parent nodes dates and and times and put those into the values.
    $parent_node = node_load($pnid);
    $val_date = array_shift($values);
    $rrule = $val_date['rrule'];
    $val_start = new DateObject($val_date['value'], $val_date['timezone']);

    $val_start = $val_start->toArray();
    $parent_value = array_shift($parent_node->{$field}[$lang]);
    $parent_start = new DateObject($parent_value['value'], $parent_value['timezone']);
    $parent_start = $parent_start->toArray();
    $parent_start['second'] = $val_start['second'];
    $parent_start['minute'] = $val_start['minute'];
    $parent_start['hour'] = $val_start['hour'];

    $parent_end = $parent_start;
    if (!empty($val_date['value2'])) {
      $val_end = new DateObject($val_date['value2'], $val_date['timezone']);
      $val_end = $val_end->toArray();
      $parent_end['second'] = $val_end['second'];
      $parent_end['minute'] = $val_end['minute'];
      $parent_end['hour'] = $val_end['hour'];
    }

    $field_info = field_info_field($field);
    $date = new DateObject($parent_start, $parent_value['timezone']);
    $parent_value['value'] = $date->format(date_type_format($field_info['type']));
    $date = new DateObject($parent_end, $parent_value['timezone']);
    $parent_value['value2'] = $date->format(date_type_format($field_info['type']));
    $parent_value['rrule'] = $rrule;
    module_load_include('inc', 'date', 'date_repeat');
    module_load_include('inc', 'date_api', 'date_api_ical');
    $rrule = date_ical_parse_rrule('', $rrule);
    $values = date_repeat_build_dates(NULL, $rrule, $field_info, $parent_value);
    $vid = $parent_node->vid;
    if ($rev) {
      $vid = NULL;
    }
    else {
      $vid = db_query('SELECT vid FROM {node} WHERE nid = :nid', array(':nid' => $parent_node->nid))->fetchField();
    }
    $parent_node = clone $node;
    $parent_node->nid = $pnid;
    $parent_node->vid = $vid;
    $parent_node->$field = array($lang => array(array_shift($values)));
    $parent_node->date_repeat_sequence[$field] = $values;
    $parent_node->auto_nodetitle_applied = FALSE;
    // node_save() results in a recursive call to this function.
    node_save($parent_node);
    return;
  }
  //We need to remove the pnid from the group as it is already loded with it's date
  unset($nids[$pnid]);
  foreach ($nids as $nid) {
    //We load the nodes in the sequence
    //Then store them in an array we will use
    //We get the of the nodes date and store it so we can compare it to a search field later
    $sequence_node = node_load($nid, NULL, TRUE);
    $nodes[$nid] = $sequence_node->vid;
    $value = array_shift($sequence_node->{$field}[$lang]);
    $date = new DateObject($value['value'], $value['timezone']);
    $field_values[$nid] = $date->format(DATE_FORMAT_DATETIME);
    unset($sequence_node);
  }
  //We sort the array of curent sequence nodes by nid as that *should* put them in date order.
  //It doesn't really matter but it should theoretically make searching quicker since it will be
  //matching nodes at the beginning of the array rather then having to search the whole thing.
  ksort($nodes);
  foreach ($values as $value) {
    //create a search value that we can unset the rrule on.
    $date = new DateObject($value['value'], $value['timezone']);
    $nid = array_search($date->format(DATE_FORMAT_DATETIME), $field_values);
    if ($nid) {
      //There is already a node with this date.
      //This prevents us from constantly creating and deleting nodes.
      //We will alwyas try to use all available nodes before creating or deleting nodes.
      $ready_nodes[$nid] = array(
        'vid' => 0,
        $field => $value,
      );
      if (!$rev) {
        $ready_nodes[$nid]['vid'] = $nodes[$nid];
      }
      unset($field_values[$nid]);
      unset($nodes[$nid]);
    }
    else {
      //there is not an existing node with this value so we will save our new one for now
      //and assign it one of the available nids later.
      $new_nodes[] = array(
        'vid' => 0,
        $field => $value,
        'new' => TRUE,
      );
    }
  }
  //At this point we should have one or two arrays of nodes. Either $new_nodes, $ready_nodes or both.
  //We also have $field_values that is loaded with the left over $nids we can grab.
  if (count($field_values) > 0 && count($new_nodes) > 0) {
    //if there are nids left to use and nodes that need them then we will use them
    foreach ($field_values as $nid => $old) {
      $ready_nodes[$nid] = array_shift($new_nodes);
      if (!$rev) {
        $vid = db_query('SELECT vid FROM {node} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
        $ready_nodes[$nid]['vid'] = $vid;
      }
      unset($ready_nodes['new']);
      unset($field_values[$nid]);
      unset($nodes[$nid]);
    }
  }
  while (count($new_nodes) > 0) {
    //if after iterating through the remaining nids available we still have new repeat nodes to add
    //then we iterate through them and add them to the $ready_nodes array without a $nid
    $ready_nodes[] = array_shift($new_nodes);
  }
  //at this point we now have all updated and new nodes saved in $ready_nodes
  //So now we will save all the $ready_nodes and then delete any remaining nodes in the $nodes array.
  foreach ($ready_nodes as $nid => $node_info) {
    $save_node = clone $node;
    $save_node->nid = $nid;
    $save_node->vid = $node_info['vid'];
    unset($save_node->date_repeat_sequence[$field]);
    $save_node->$field = array($lang => array($node_info[$field]));
    $save_node->auto_nodetitle_applied = FALSE;
    if (!empty($node_info['new'])) {
      $save_node->nid = 0;
      $save_node->is_new = TRUE;
      unset($save_node->vid);
      node_save($save_node);
      //if the $save_node->nid was preexisting then we simply updated and don't need to add a new record to the sequence table
      //If the $save_node->nid was empty then it was a new node and needs to be saved to the sequence table
      $record = array(
        'sqid' => $sqid,
        'nid' => $save_node->nid,
      );
      drupal_write_record('date_repeat_sequence_node', $record);
    }
    else {
      node_save($save_node);
    }
    unset($save_node);
  }
  //Next if there are any nodes left in the $nodes array they need to be deleted.
  if (count($nodes) > 0) {
    foreach ($nodes as $nid => $vid) {
      $delete_node = node_load($nid, NULL, TRUE);
      $edit_name = "date_repeat_sequence_edit__$field";
      $delete_node->$edit_name = 'only';
      _drs_node_delete($delete_node);
    }
  }
}

/**
 * Removes a node from the sequence it is in and resets the rrule for all other nodes in that sequence.
 *
 * @param $node
 *   The Node object
 *
 * @param $field
 *   The sequence Field name
 */
function _date_repeat_sequence_node_update_only($node, $field) {
  $sqid = $node->date_repeat_sequences[$field]['sqid'];
  $nids = date_repeat_sequence_load_nids($sqid);
  if (count($nids) < 2) {
    //do nothing as this is the only node in this sequence already.
    return;
  }
  if ($node->nid == $node->date_repeat_sequences[$field]['pnid']) {
    //this is the parent node so we do not need to update the rrule just remove the parent node and shift the
    //Sequence parent to the next node in the sequence
    date_repeat_sequence_shift_parent($node->date_repeat_sequences[$field]);
  }
  //We can delete this node from the sequence
  /* db_query('DELETE FROM {date_repeat_sequence_node} WHERE sqid = %d AND nid = %d', $sqid, $node->nid) */
  db_delete('date_repeat_sequence_node')
    ->condition('sqid', $sqid)
    ->condition('nid', $node->nid)
    ->execute();

  //also remove the rrule from the db
  $info = field_info_field($field);
  $sql_info = $info['storage']['details']['sql'][FIELD_LOAD_REVISION];
  reset($sql_info);
  $table = key($sql_info);
  /* db_query('UPDATE {%s} SET %s = "RRULE:FREQ=NONE;INTERVAL=0;" WHERE vid = %d') */
  db_update($table)
    ->fields(array(
      $sql_info[$table]['rrule'] => "RRULE:FREQ=NONE;INTERVAL=0;",
    ))
    ->condition('revision_id', $node->vid)
    ->execute();
}

/**
 * Saves an existing repeat sequence for all nodes in the future of the sequence
 *
 * @param $node
 *   The Node object
 *
 * @param $field
 *   The sequence Field name
 */
function _date_repeat_sequence_node_update_future($node, $field) {
  $sqid = $node->date_repeat_sequences[$field]['sqid'];
  $pnid = $node->date_repeat_sequences[$field]['pnid'];
  if ($node->nid == $pnid) {
    //since this is the parent nid we can just run it through the update all sequence
    _date_repeat_sequence_node_update_all($node, $field);
    return;
  }
  module_load_include('inc', 'date_api', 'date_api_sql');
  $lang = empty($node->language) ? LANGUAGE_NONE : $node->language;
  $value = array_shift($node->{$field}[$lang]);
  $date_sql = new date_sql_handler;
  $info = field_info_field($field);
  $sql_info = $info['storage']['details']['sql'][FIELD_LOAD_REVISION];
  reset($sql_info);
  $table = key($sql_info);
  $column = array_shift($sql_info[$table]);
  $sql = 'SELECT n.nid FROM {date_repeat_sequence_node} dn
    JOIN {node} n ON n.nid = dn.nid
    JOIN {' . $table . '} c ON c.revision_id = n.vid
    WHERE dn.sqid = :sqid';
  $sql .= ' AND ' . $date_sql->sql_where_date('FIELD', 'c.' . $column, '>', ':date');
  $sql .= ' ORDER BY c.' . $column . ' ASC';
  $results = db_query($sql, array(':sqid' => $sqid, ':date' => $value['value']));
  //initiate the $nids array with the current nid
  $nids[$node->nid] = $node->nid;
  while ($nid = $results->fetchField()) {
    $nids[$nid] = $nid;
  }
  // Create our new sequence.
  $seq = array(
    'pnid' => $node->nid,
    'field' => $field,
  );
  drupal_write_record('date_repeat_sequence', $seq);
  // Update the nodes in this sequence
  /* db_query('UPDATE {date_repeat_sequence_node} SET sqid = %d WHERE sqid = %d AND nid IN (%s)', $seq['sqid'], $sqid, implode(',', $nids)) */
  db_update('date_repeat_sequence_node')
    ->fields(array(
      'sqid' => $seq['sqid'],
    ))
    ->condition('sqid', $sqid)
    ->condition('nid', $nids, 'IN')
    ->execute();

  $node->date_repeat_sequences[$field] = $seq;
  //now lets run it through the update_all function for this new sequence
  _date_repeat_sequence_node_update_all($node, $field);
}

/**
 * Deletes a node's date_repeats sequences
 */
function _date_repeat_sequence_node_delete(&$node) {
  static $processing = array();
  if (in_array($node->nid, $processing)) {
    //If the nid is in our processing array then we called node_delete
    //and therefore do not need to rerun through this.
    return;
  }
  $sequences = $node->date_repeat_sequences;
  if (!is_array($sequences)) {
    return;
  }
  $lang = empty($node->language) ? LANGUAGE_NONE : $node->language;
  foreach ($sequences as $field => $values) {
    $field_settings = field_info_instance('node', $field, $node->type);
    $edit_name = "date_repeat_sequence_edit__$field";
    $update_op = empty($node->$edit_name) ? $field_settings['settings']['sequence']['edit_default'] : $node->$edit_name;
    $sqid = $node->date_repeat_sequences[$field]['sqid'];
    $seq = $node->date_repeat_sequences[$field];
    if ($seq['pnid'] == $node->nid && $update_op == 'future') {
      //if this is the parent node and the command was to delete this and all future nodes
      //then we need to kill the whole sequence
      $update_op = 'all';
    }
    switch ($update_op) {
      case 'all':
        $nids = date_repeat_sequence_load_nids($sqid);
        foreach ($nids as $nid) {
          if ($nid != $node->nid) {
            $processing[$nid] = $nid;
            //so we don't get stuck in an infinite loop
            node_delete($nid);
          }
        }
        /* db_query('DELETE FROM {date_repeat_sequence} WHERE sqid = %d', $sqid) */
        db_delete('date_repeat_sequence')
          ->condition('sqid', $sqid)
          ->execute();
        /* db_query('DELETE FROM {date_repeat_sequence_node} WHERE sqid = %d', $sqid) */
        db_delete('date_repeat_sequence_node')
          ->condition('sqid', $sqid)
          ->execute();
        break;

      case 'only':
      default:
        if ($seq['pnid'] == $node->nid) {
          //if this is the parent nid and we are killing it then we need to shift the sequence to a new pnid
          date_repeat_sequence_shift_parent($seq);
        }
        /* db_query('DELETE FROM {date_repeat_sequence_node} WHERE sqid = %d AND nid = %d', $sqid, $node->nid) */
        db_delete('date_repeat_sequence_node')
          ->condition('sqid', $sqid)
          ->condition('nid', $node->nid)
          ->execute();
        break;

      case 'future':
        $nids = array($node->nid => $node->nid); //Set it by default to use the current node
        module_load_include('inc', 'date_api', 'date_api_sql');
        $value = array_shift($node->{$field}[$lang]);
        $date_sql = new date_sql_handler;
        $info = field_info_field($field);
        $sql_info = $info['storage']['details']['sql'][FIELD_LOAD_REVISION];
        reset($sql_info);
        $table = key($sql_info);
        $column = array_shift($sql_info[$table]);
        $sql = 'SELECT n.nid FROM {date_repeat_sequence_node} dn
          JOIN {node} n ON n.nid = dn.nid
          JOIN {' . $table . '} c ON c.revision_id = n.vid
          WHERE dn.sqid = :sqid';
        $sql .= ' AND ' . $date_sql->sql_where_date('DATE', 'c.' . $column, '>=', $value['value']);
        $sql .= ' ORDER BY c.' . $column . ' ASC';
        $results = db_query($sql, array(':sqid' => $sqid));
        while ($nid = $results->fetchField()) {
          $nids[$nid] = $nid;
          $processing[$nid] = $nid;
          //so we don't get stuck in an infinite loop
          node_delete($nid);
        }
        /* db_query('DELETE FROM {date_repeat_sequence_node} WHERE sqid = %d AND nid IN (%s)', $sqid, implode(',', $nids)) */
        db_delete('date_repeat_sequence_node')
          ->condition('sqid', $sqid)
          ->condition('nid', $nids, 'IN')
          ->execute();
        break;
    }
  }
}

/**
 * Shifts the node sequence off of the current parent and on to the next date in that sequence
 *
 * @param $seq
 *   The sequence array. Keys are sqid, pnid, & field.
 */
function date_repeat_sequence_shift_parent($seq) {
  $info = field_info_field($seq['field']);
  $sql_info = $info['storage']['details']['sql'][FIELD_LOAD_REVISION];
  reset($sql_info);
  $table = key($sql_info);
  $column = array_shift($sql_info[$table]);
  $sql = 'SELECT n.nid FROM {date_repeat_sequence_node} dn
    JOIN {node} n ON n.nid = dn.nid
    JOIN {' . $table . '} c ON c.revision_id = n.vid
    WHERE dn.sqid = :sqid AND dn.nid != :pnid
    ORDER BY c.' . $column . ' ASC';
  $nid = db_query($sql, array(':sqid' => $seq['sqid'], ':pnid' => $seq['pnid']))->fetchField();
  if ($nid) {
    $seq['pnid'] = $nid;
    drupal_write_record('date_repeat_sequence', $seq, 'sqid');
  }
  else {
    /* db_query('DELETE FROM {date_repeat_sequence} WHERE sqid = %d', $seq['sqid']) */
    db_delete('date_repeat_sequence')
      ->condition('sqid', $seq['sqid'])
      ->execute();
    /* db_query('DELETE FROM {date_repeat_sequence_node} WHERE sqid = %d', $seq['sqid']) */
    db_delete('date_repeat_sequence_node')
      ->condition('sqid', $seq['sqid'])
      ->execute();
  }
}

/**
 * Mimicks the actual node_delete except this takes an already loaded $node
 * as the argument so we can pass extra values into the function
 *
 * @see node_delete()
 */
function _drs_node_delete($node) {
  // Clear the cache before the load, so if multiple nodes are deleted, the
  // memory will not fill up with nodes (possibly) already removed.
  node_load(NULL, NULL, TRUE);

  if (node_access('delete', $node)) {
    /* db_query('DELETE FROM {node} WHERE nid = %d', $node->nid) */
    db_delete('node')
      ->condition('nid', $node->nid)
      ->execute();
    /* db_query('DELETE FROM {node_revision} WHERE nid = %d', $node->nid) */
    db_delete('node_revision')
      ->condition('nid', $node->nid)
      ->execute();
    /* db_query('DELETE FROM {node_access} WHERE nid = %d', $node->nid) */
    db_delete('node_access')
      ->condition('nid', $node->nid)
      ->execute();

    // Call the node-specific callback (if any):
    node_invoke($node, 'delete');
    module_invoke_all('node_delete', $node);

    // Clear the page and block caches.
    cache_clear_all();

    // Remove this node from the search index if needed.
    if (function_exists('search_wipe')) {
      search_wipe($node->nid, 'node');
    }
    watchdog('content', '@type: deleted %title.', array('@type' => $node->type, '%title' => $node->title));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => node_type_get_name($node), '%title' => $node->title)));
  }
}
