(function ($) {
  Drupal.behaviors.dateRepeatSequence = {
    attach: function(context, settings) {
      $('.node-form').bind('submit', function(e) {
        if (dateRepeatSequence.active == false) {
          dateRepeatSequence.clicked = $('input[clicked=true]');
          dateRepeatSequence.open();
          e.preventDefault();
          return false;
        }
      });
      $('input[name="op"]').live('click', function() {
        $('input[name="op"]').attr('clicked', false);
        $(this).attr('clicked', true);
      });
      dateRepeatSequence.init();
      dateRepeatSequence.form = '.node-form';
    }
  };

  var dateRepeatSequence = {
    active: false,
    clicked: false,
    dia: '#drs-dialog',
    form: null,

    $dialog: function(arg) {
      $('#drs-dialog').dialog(arg);
    },

    init: function() {
      dateRepeatSequence.buildContent();
      dateRepeatSequence.$dialog({
        autoOpen: false,
        draggable: false,
        modal: true,
        resizable: false,
        closeText: '',
        title: Drupal.t('Edit Occurrence'),
        buttons:
        [ {
            text: Drupal.t('Continue'),
            click: function() {
              dateRepeatSequence.$dialog('close');
              dateRepeatSequence.saveForm();
            }
          }, {
            text: Drupal.t('Cancel'),
            click: function() {
              dateRepeatSequence.$dialog('close');
            }
          }
        ],
        open: function() {
          $(this).parent().find('button:first').focus();
        }
      });
    },

    buildContent: function() {
      fields = Drupal.settings.date_repeat_seq.fields;
      for (field in fields) {
        label = fields[field];
        title = $('<h4></h4>').text(label).appendTo(dateRepeatSequence.dia);
        $parent = $('div.seq-save-' + field).parent('.form-item');
        $clone = $('div.seq-save-' + field).clone();
        $clone.appendTo(dateRepeatSequence.dia);
        $parent.hide();
      }
    },

    open: function() {
      dateRepeatSequence.$dialog('open');
    },

    saveForm: function() {
      $(dateRepeatSequence.dia).css('display','none').appendTo(dateRepeatSequence.form);
      dateRepeatSequence.active = true;
      dateRepeatSequence.clicked.trigger('click');
    },
  };
})(jQuery);
