(function ($) {
  Drupal.behaviors.drsAdmin = {
    attach: function(context, settings) {
      $('input.seq-admin-chb').change(function() {
        if ($(this).is(':checked')) {
          $('.sequence-options').slideDown('fast');
        }
        else {
          $('.sequence-options').slideUp('fast');
        }
      });
      if (!$('input.seq-admin-chb').is(':checked')) {
        $('.sequence-options').hide();
      }
    }
  };
})(jQuery);
